package com.agoda.services.resources;

import com.agoda.services.enums.ResponseStatus;
import com.agoda.services.models.response.ApiResponse;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Prashanth Sivasankaran on 4/27/17.
 */
public abstract class AbstractResource {
    public Response ok() {
        return ok(null);
    }

    public Response ok(Object data) {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setData(data);
        apiResponse.setTimeStamp(System.currentTimeMillis());
        apiResponse.setStatus(ResponseStatus.SUCCESS.name());
        return Response.status(Response.Status.OK).type(MediaType.APPLICATION_JSON).entity(apiResponse).build();
    }

    public Response fail(Response.Status status, Object data) {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setData(data);
        apiResponse.setTimeStamp(System.currentTimeMillis());
        apiResponse.setStatus(ResponseStatus.FAILURE.name());
        apiResponse.setStatusCode(ResponseStatus.FAILURE.getCode());
        return Response.status(status).type(MediaType.APPLICATION_JSON).entity(apiResponse).build();
    }
}
