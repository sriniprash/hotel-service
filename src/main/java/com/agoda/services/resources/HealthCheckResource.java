package com.agoda.services.resources;


import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Prashanth Sivasankaran on 4/27/17.
 */

@Path("/healthcheck")
@Slf4j
@Produces(MediaType.APPLICATION_JSON)
public class HealthCheckResource extends AbstractResource {
    @GET
    public Response healthCheck() {
        return ok("HEALTHY");
    }
}
