package com.agoda.services.resources;

import com.agoda.services.core.ISearchService;
import com.agoda.services.enums.SortOrder;
import com.agoda.services.filters.namebindings.RateLimited;
import com.google.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.NotEmpty;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Prashanth Sivasankaran on 4/27/17.
 */
@Slf4j
@Produces(MediaType.APPLICATION_JSON)
@Path("/v1/search")
@RateLimited
public class SearchResource extends AbstractResource {

    private ISearchService searchService;

    @Inject
    public SearchResource(ISearchService searchService) {
        this.searchService = searchService;
    }

    @GET
    public Response searchHotelsByCity(@NotEmpty @QueryParam("city") String city, @QueryParam("priceSortOrder") SortOrder priceSortOrder) {
        if(priceSortOrder == null) priceSortOrder = SortOrder.NONE;
        try {
            return ok(this.searchService.searchByCity(city, priceSortOrder));
        }
        catch (Exception e) {
            log.error("Error in search API", e);
            return fail(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

}
