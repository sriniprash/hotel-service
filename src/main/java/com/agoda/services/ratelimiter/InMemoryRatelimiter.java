package com.agoda.services.ratelimiter;


import com.agoda.services.HotelServiceConfiguration;
import org.apache.commons.collections4.queue.CircularFifoQueue;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Prashanth Sivasankaran on 4/28/17.
 */
public class InMemoryRatelimiter implements IRatelimiter {
    private String key;
    private int numRequests;
    private int intervalInSeconds;
    private int blockIntervalInSeconds;
    private CircularFifoQueue<Long> tokenQueue;
    private boolean inBlockMode;

    public InMemoryRatelimiter(String key, HotelServiceConfiguration.RateLimitConfig rateLimitConfig) {
        this.key = key;
        this.numRequests = rateLimitConfig.getNumRequests();
        this.intervalInSeconds = rateLimitConfig.getIntervalInSeconds();
        this.blockIntervalInSeconds = rateLimitConfig.getBlockIntervalInSeconds();
        this.tokenQueue = new CircularFifoQueue<>(this.numRequests);
        this.inBlockMode = false;

    }
    @Override
    public synchronized boolean checkLimit() {
        if(this.inBlockMode) {
            return false;
        }
        Long currentTime = System.currentTimeMillis();
        if(!this.tokenQueue.isAtFullCapacity()) {
            this.tokenQueue.add(currentTime);
            return true;
        }
        else {
            Long oldestEntry = this.tokenQueue.remove();
            if((currentTime - oldestEntry) <= this.intervalInSeconds * 1000) {
                // Limit has reached
                this.inBlockMode = true;
                BlockTimerTask timerTask = new BlockTimerTask();
                Timer timer = new Timer(true);
                timer.schedule(timerTask,this.blockIntervalInSeconds * 1000);
                return false;
            }
        }
        return true;
    }

    private class BlockTimerTask extends TimerTask {
        @Override
        public void run() {
            InMemoryRatelimiter.this.inBlockMode = false;
            InMemoryRatelimiter.this.tokenQueue.clear();
        }
    }
}
