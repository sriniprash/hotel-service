package com.agoda.services.ratelimiter;

/**
 * Created by Prashanth Sivasankaran on 4/28/17.
 */
public interface IRatelimiter {
    boolean checkLimit();
}
