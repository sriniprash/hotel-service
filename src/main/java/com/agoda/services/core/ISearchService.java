package com.agoda.services.core;

import com.agoda.services.enums.SortOrder;
import com.agoda.services.models.dto.HotelDto;

import java.util.List;

/**
 * Created by Prashanth Sivasankaran on 4/27/17.
 */
public interface ISearchService {
    List<HotelDto> searchByCity(String city, SortOrder sortOrder);
}
