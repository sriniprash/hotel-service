package com.agoda.services.core;

import com.agoda.services.enums.SortOrder;
import com.agoda.services.models.dto.HotelDto;
import com.csvreader.CsvReader;
import com.google.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Prashanth Sivasankaran on 4/27/17.
 */
@Slf4j
public class CsvBasedSearchServiceImpl implements ISearchService {
    private static final String CITY = "CITY";
    private static final String HOTEL = "HOTELID";
    private static final String ROOM = "ROOM";
    private static final String PRICE = "PRICE";

    private static final String filePath = "hoteldb.csv";

    private List<HotelDto> allHotels;

    @Inject
    public CsvBasedSearchServiceImpl()  {
        allHotels = new ArrayList<>();
        InputStream inputStream = null;
        try {
            inputStream = getClass().getClassLoader().getResourceAsStream(filePath);
            CsvReader hotels = new CsvReader(inputStream, Charset.defaultCharset());
            // Consume the CSV headers.
            hotels.readHeaders();
            // Read each record one by one
            while(hotels.readRecord()) {
                HotelDto hotel = new HotelDto();
                hotel.setCity(hotels.get(CITY));
                hotel.setId(Integer.parseInt(hotels.get(HOTEL)));
                hotel.setPrice(Float.parseFloat(hotels.get(PRICE)));
                hotel.setRoom(hotels.get(ROOM));
                allHotels.add(hotel);
            }
        }
        catch (Exception e) {
            log.error("Error while parsing CSV", e);
        }
    }

    @Override
    public List<HotelDto> searchByCity(final String city, SortOrder sortOrder) {
        log.info("Calling Search Service for city: {} with sortOrder: {}", city, sortOrder);
        List<HotelDto> hotelDtoList = allHotels.stream().filter
                (hotel -> StringUtils.equalsIgnoreCase(city, hotel.getCity())).collect(Collectors.toList());

        switch (sortOrder) {
            case ASC:
                Collections.sort(hotelDtoList, (a, b) ->  (int) (a.getPrice() - b.getPrice()));
                break;
            case DESC:
                Collections.sort(hotelDtoList, (a, b) ->  -1 * (int) (a.getPrice() - b.getPrice()));
                break;
        }
        return hotelDtoList;
    }
}
