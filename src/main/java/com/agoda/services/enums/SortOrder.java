package com.agoda.services.enums;

/**
 * Created by Prashanth Sivasankaran on 4/27/17.
 */
public enum SortOrder {
    ASC, DESC, NONE
}
