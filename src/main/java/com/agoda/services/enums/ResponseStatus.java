package com.agoda.services.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by Prashanth Sivasankaran on 4/27/17.
 */
@AllArgsConstructor
@Getter
public enum ResponseStatus {
    SUCCESS ("SUCCESS"),
    FAILURE ("FAILURE");

    private final String code;
}
