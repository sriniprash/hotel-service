package com.agoda.services.filters.namebindings;

import javax.ws.rs.NameBinding;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Prashanth Sivasankaran on 4/28/17.
 */
@NameBinding
@Retention(RetentionPolicy.RUNTIME)
public @interface RateLimited {
}
