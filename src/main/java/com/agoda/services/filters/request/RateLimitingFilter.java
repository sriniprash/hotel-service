package com.agoda.services.filters.request;

import com.agoda.services.HotelServiceConfiguration;
import com.agoda.services.constants.HeaderConstants;
import com.agoda.services.filters.namebindings.RateLimited;
import com.agoda.services.ratelimiter.IRatelimiter;
import com.agoda.services.ratelimiter.InMemoryRatelimiter;
import com.google.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Prashanth Sivasankaran on 4/28/17.
 */

@Provider
@Priority(Priorities.AUTHORIZATION)
@RateLimited
@Slf4j
public class RateLimitingFilter implements ContainerRequestFilter {
    private HotelServiceConfiguration configuration;
    private static Map<String, IRatelimiter> rateLimitingFilterMap = new HashMap<>();


    @Inject
    public RateLimitingFilter(HotelServiceConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        String apiKey = requestContext.getHeaderString(HeaderConstants.API_KEY);
        log.info("Api key: {}", apiKey);
        IRatelimiter rateLimiter = rateLimitingFilterMap.get(apiKey);
        if(rateLimiter == null) {
            HotelServiceConfiguration.RateLimitConfig rateLimitConfig = null;
            if(this.configuration.getRateLimitConfigs().containsKey(apiKey)) {
                rateLimitConfig = this.configuration.getRateLimitConfigs().get(apiKey);
            }
            else {
                rateLimitConfig = this.configuration.getGlobalRateLimitConfig();
            }
            rateLimiter = new InMemoryRatelimiter(apiKey, rateLimitConfig);
            rateLimitingFilterMap.put(apiKey, rateLimiter);
        }
        if(!rateLimiter.checkLimit()) {
            log.info("Ratelimiting API key :{} for");
            requestContext.abortWith(Response.status(429)
                    .entity("Too many requests for Api key: " + apiKey).type(MediaType.APPLICATION_JSON).build());
            return;
        }
    }
}

