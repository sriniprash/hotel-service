package com.agoda.services;

import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.validator.constraints.*;
import javax.validation.constraints.*;
import java.util.Map;

@Data
public class HotelServiceConfiguration extends Configuration {
    @Data
    public static class RateLimitConfig {
        private int numRequests;
        private int intervalInSeconds;
        private int blockIntervalInSeconds;
    }

    @NotNull
    private Map<String, RateLimitConfig> rateLimitConfigs;
    @NotNull
    private RateLimitConfig globalRateLimitConfig;
}
