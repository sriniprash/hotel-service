package com.agoda.services.guice;

import com.agoda.services.core.CsvBasedSearchServiceImpl;
import com.agoda.services.core.ISearchService;
import com.google.inject.AbstractModule;

/**
 * Created by Prashanth Sivasankaran on 4/27/17.
 */
public class HotelServiceGuiceModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(ISearchService.class).to(CsvBasedSearchServiceImpl.class).asEagerSingleton();
    }
}
