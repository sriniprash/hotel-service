package com.agoda.services.models.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

/**
 * Created by Prashanth Sivasankaran on 4/27/17.
 */
@Data
public class ApiResponse {
    @JsonInclude(Include.NON_NULL)
    private Object data;

    @JsonInclude(Include.NON_NULL)
    private Long timeStamp;

    @JsonInclude(Include.NON_NULL)
    private String status;

    @JsonInclude(Include.NON_NULL)
    private String statusCode;
}
