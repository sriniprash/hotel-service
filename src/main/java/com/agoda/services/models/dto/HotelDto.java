package com.agoda.services.models.dto;

import lombok.Data;

/**
 * Created by Prashanth Sivasankaran on 4/27/17.
 */
@Data
public class HotelDto {
    private int id;
    private String city;
    private String room;
    private float price;
}
