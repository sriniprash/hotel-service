package com.agoda.services;

import com.agoda.services.guice.HotelServiceGuiceModule;
import com.google.inject.Stage;
import com.hubspot.dropwizard.guice.GuiceBundle;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class HotelServiceApplication extends Application<HotelServiceConfiguration> {

    public static void main(final String[] args) throws Exception {
        new HotelServiceApplication().run(args);
    }

    @Override
    public String getName() {
        return "HotelService";
    }

    @Override
    public void initialize(final Bootstrap<HotelServiceConfiguration> bootstrap) {
        bootstrap.addBundle(GuiceBundle.<HotelServiceConfiguration>newBuilder()
            .enableAutoConfig(getClass().getPackage().getName())
            .setConfigClass(HotelServiceConfiguration.class)
            .addModule(new HotelServiceGuiceModule())
            .build(Stage.PRODUCTION));
    }

    @Override
    public void run(final HotelServiceConfiguration configuration,
                    final Environment environment) {
        // TODO: implement application
    }

}
